# 班级：20级软工1班 学号：202010424122 姓名：王玉婷
# 实验4：PL/SQL语言打印杨辉三角

## 实验目的

掌握Oracle PL/SQL语言以及存储过程的编写。

## 实验内容

- 认真阅读并运行下面的杨辉三角源代码。
- 将源代码转为hr用户下的一个存储过程Procedure，名称为YHTriangle，存储起来。存储过程接受行数N作为参数。
- 运行这个存储过程即可以打印出N行杨辉三角。
- 写出创建YHTriangle的SQL语句。

## 实验步骤

### 1.阅读杨辉三角源代码，在oracle中运行

```sql
set serveroutput on;
declare
type t_number is varray (100) of integer not null; --数组
i integer;
j integer;
spaces varchar2(30) :='   '; --三个空格，用于打印时分隔数字
N integer := 9; -- 一共打印9行数字
rowArray t_number := t_number();
begin
    dbms_output.put_line('1'); --先打印第1行
    dbms_output.put(rpad(1,9,' '));--先打印第2行
    dbms_output.put(rpad(1,9,' '));--打印第一个1
    dbms_output.put_line(''); --打印换行
    --初始化数组数据
    for i in 1 .. N loop
        rowArray.extend;
    end loop;
    rowArray(1):=1;
    rowArray(2):=1;    
    for i in 3 .. N --打印每行，从第3行起
    loop
        rowArray(i):=1;    
        j:=i-1;
            --准备第j行的数组数据，第j行数据有j个数字，第1和第j个数字为1
            --这里从第j-1个数字循环到第2个数字，顺序是从右到左
        while j>1 
        loop
            rowArray(j):=rowArray(j)+rowArray(j-1);
            j:=j-1;
        end loop;
            --打印第i行
        for j in 1 .. i
        loop
            dbms_output.put(rpad(rowArray(j),9,' '));--打印第一个1
        end loop;
        dbms_output.put_line(''); --打印换行
    end loop;
END;
```
![](1.png)
### 2.创建存储过程YHTriangle

```
CREATE OR REPLACE PROCEDURE YHTriangle(N IN INTEGER) AS
    type t_number is varray (100) of integer not null; --数组
    i integer;
    j integer;
    spaces varchar2(30) :='   '; --三个空格，用于打印时分隔数字
    rowArray t_number := t_number();
BEGIN
    dbms_output.put_line('1'); --先打印第1行
    dbms_output.put(rpad(1,9,' '));--先打印第2行
    dbms_output.put(rpad(1,9,' '));--打印第一个1
    dbms_output.put_line(''); --打印换行
    --初始化数组数据
    FOR i IN 1 .. N LOOP
        rowArray.extend;
    END LOOP;
    rowArray(1) := 1;
    rowArray(2) := 1;    
    FOR i IN 3 .. N --打印每行，从第3行起
    LOOP
        rowArray(i) := 1;    
        j := i-1;
        --准备第j行的数组数据，第j行数据有j个数字，第1和第j个数字为1
        --这里从第j-1个数字循环到第2个数字，顺序是从右到左
        WHILE j > 1 LOOP
            rowArray(j) := rowArray(j) + rowArray(j-1);
            j := j-1;
        END LOOP;
        --打印第i行
        FOR j IN 1 .. i LOOP
            dbms_output.put(rpad(rowArray(j),9,' '));--打印第一个1
        END LOOP;
        dbms_output.put_line(''); --打印换行
    END LOOP;
END YHTriangle;
```
![](2.png)
### 3.打印10行杨辉三角
```
BEGIN
  YHTriangle(10);
END;
```
![](3.png)
## 实验总结和心得体会
本次实验是关于Oracle PL/SQL语言和存储过程的编写，通过对源代码杨辉三角的学习和转化为存储过程，我学会了使用PL/SQL语言编写函数和存储过程，掌握了PL/SQL语言的基本语法和存储过程的编写和调用方法,包括变量声明、控制结构、游标、异常处理等内容，并且了解了如何在Oracle数据库中创建、编辑和运行存储过程。
在本次实验中，我学习了Oracle PL/SQL语言和存储过程的基础知识，通过实验，我深刻理解了存储过程的优点，例如可重用性、简化复杂的SQL语句、提高性能等。