# 班级：20级软工1班 学号：202010424122 姓名：王玉婷
# 实验3：创建分区表

## 实验目的

掌握分区表的创建方法，掌握各种分区方式的使用场景。

## 实验内容

- 本实验使用实验2的sale用户创建两张表：订单表(orders)与订单详表(order_details)。
- 两个表通过列order_id建立主外键关联。给表orders.customer_name增加B_Tree索引。
- 新建两个序列，分别设置orders.order_id和order_details.id，插入数据的时候，不需要手工设置这两个ID值。
- orders表按订单日期（order_date）设置范围分区。
- order_details表设置引用分区。
- 表创建成功后，插入数据，数据应该能并平均分布到各个分区。orders表的数据都大于40万行，order_details表的数据大于200万行（每个订单对应5个order_details）。
- 写出插入数据的脚本和两个表的联合查询的语句，并分析语句的执行计划。
- 进行分区与不分区的对比实验。

## 实验步骤

### 1.创建订单表（orders），按照订单日期（order_date）设置范围分区
```
-- 创建 orders 表
CREATE TABLE orders (
    order_id NUMBER(10) NOT NULL PRIMARY KEY,
    customer_name VARCHAR2(50) NOT NULL,
    order_date DATE NOT NULL,
    discount NUMBER(4,2),
    employee_id NUMBER(6)
)

--创建表的一些参数设置--
TABLESPACE USERS 
PCTFREE 10 INITRANS 1 
STORAGE (   BUFFER_POOL DEFAULT ) 
NOCOMPRESS NOPARALLEL 

--创建order表的分区--
PARTITION BY RANGE (order_date) (
    PARTITION PARTITION_BEFORE_2016 VALUES LESS THAN (TO_DATE('2019-01-01', 'YYYY-MM-DD')) NOLOGGING TABLESPACE USERS,
    PARTITION PARTITION_BEFORE_2020 VALUES LESS THAN (TO_DATE('2020-01-01', 'YYYY-MM-DD')) NOLOGGING TABLESPACE USERS,
    PARTITION PARTITION_BEFORE_2021 VALUES LESS THAN (TO_DATE('2021-01-01', 'YYYY-MM-DD')) NOLOGGING TABLESPACE USERS
);

--以后再逐年增加新年份的分区--
ALTER TABLE orders ADD PARTITION partition_before_2022
VALUES LESS THAN(TO_DATE('2022-01-01','YYYY-MM-DD'))
TABLESPACE USERS;
```
![](1.png)
![](2.png)
### 2.创建订单详情表（order_details），设置引用分区
```
--创建订单详情表--
CREATE TABLE order_details
(
id NUMBER(9, 0) NOT NULL 
, order_id NUMBER(10, 0) NOT NULL
, product_id VARCHAR2(40 BYTE) NOT NULL 
, product_num NUMBER(8, 2) NOT NULL 
, product_price NUMBER(8, 2) NOT NULL 
, CONSTRAINT ORDER_DETAILS_PK PRIMARY KEY 
  (
    id 
  )
, CONSTRAINT order_details_fk1 FOREIGN KEY  (order_id)
REFERENCES orders  (  order_id   )
ENABLE
) 
--参数设置--
TABLESPACE USERS 
PCTFREE 10 INITRANS 1 
STORAGE ( BUFFER_POOL DEFAULT ) 
NOCOMPRESS NOPARALLEL
PARTITION BY REFERENCE (order_details_fk1);

```
![](3.png)
### 3.新建两个序列，分别设置orders.order_id和order_details.id
```
-- 创建序列 order_order_id
CREATE SEQUENCE order_order_id 
  MINVALUE 1 MAXVALUE 999999999 INCREMENT BY 1 START WITH 1
  CACHE 20 NOORDER NOCYCLE NOKEEP NOSCALE GLOBAL;

-- 创建序列 order_details_seq
CREATE SEQUENCE order_details_seq
  MINVALUE 1 MAXVALUE 999999999 INCREMENT BY 1 START WITH 1
  CACHE 20 NOORDER NOCYCLE NOKEEP NOSCALE GLOBAL;
```
![](4.png)
### 4.给order_customer_name增加索引
```
CREATE INDEX order_customer_name ON oders(customer_name)
TABLESPACE USERS
```
![](5.png)
### 5.插入数据
```
-- 插入 orders 表数据
DECLARE
    i NUMBER(6) := 0;
BEGIN
    LOOP
        EXIT WHEN i >= 2000;
        i := i + 1;
        INSERT INTO orders(order_id, customer_name, order_date, discount, employee_id)
        VALUES(order_orders_seq.NEXTVAL, 'customer'||MOD(i,100), DATE '2022-01-01'+TRUNC(DBMS_RANDOM.VALUE(0,365)), TRUNC(DBMS_RANDOM.VALUE(0,100))/100, MOD(i, 10));
    END LOOP;
    COMMIT;
END;

-- 插入 order_details 表数据
DECLARE
    i NUMBER(6) := 0;
BEGIN
    LOOP
        EXIT WHEN i >= 10000;
        i := i + 1;
        INSERT INTO order_details(order_id, product_id, product_num,product_price)
        VALUES(order_details_id.NEXTVAL, MOD(i,2000)+1, MOD(i,100)+1, 'product'||MOD(i,100));
    END LOOP;
    COMMIT;
END;
```
![](6.png)
![](7.png)
### 6.写出插入数据的脚本和两个表的联合查询的语句，并分析语句的执行计划
```
SELECT o.order_id, o.customer_name, o.order_date, od.product_id, od.product_num, od.product_price
FROM orders o
JOIN order_details od
ON o.order_id = od.order_id;
```
![](8.png)


## 实验结论和总结
结论:
1.分区表的优势在于当处理大量数据时，可以提高查询速度，尤其是在需要查询的数据仅在一个分区时。
2.分区表相对于非分区表，在数据量较大时，插入和查询的效率都更高。
3.在本实验中，使用分区表查询的效率优于不分区表。
<p>
总结:
通过这次实验，我对Oracle数据库的分区表有了更深入的了解和实践。在实验中，我学习到了如何创建分区表、设置分区键和分区方式，以及如何插入和查询分区表的数据。同时，我还学习到了如何使用序列来自动生成主键和外键值，以及如何创建索引来提高查询效率。

通过实验的对比分析，我发现使用分区表相对于不分区表，在处理大量数据时具有明显的优势，能够显著提高查询效率和数据插入速度。此外，分区表还能够在处理不同范围的数据时，提高查询效率。

在实际应用中，我们需要根据实际业务情况来选择是否需要使用分区表。如果数据量较大、查询需求较高或需要处理不同范围的数据时，建议使用分区表。同时，在使用分区表时，我们需要仔细考虑分区键和分区方式的选择，以及索引的创建和使用，以确保查询效率的最大化。

总的来说，这次实验对我的数据库知识水平有很大的提高和帮助，我也更加深入地了解了分区表的优势和应用场景。
