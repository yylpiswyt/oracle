# 班级：20级软工1班 学号：202010424122 姓名：王玉婷
# 实验6（期末考核）： 基于Oracle数据库的商品销售系统的设计
## 考核内容
- 设计一套基于Oracle数据库的商品销售系统的数据库设计方案。
  - 表及表空间设计方案。至少两个表空间，至少4张表，总的模拟数据量不少于10万条。
  - 设计权限及用户分配方案。至少两个用户。
  - 在数据库中建立一个程序包，在包中用PL/SQL语言设计一些存储过程和函数，实现比较复杂的业务逻辑。
  - 设计一套数据库的备份方案。
## 完成步骤
### 1.表及表空间设计方案。至少两个表空间，至少4张表，总的模拟数据量不少于10万条。
(1)使用pdborcl_hr用户完成本次设计，给hr用户授予创建表空间的权限
![](./img/1.png)

(2)创建两个表空间（tablespace）：ts1和ts2
```
-- 创建表空间 ts1
CREATE TABLESPACE ts1
DATAFILE 'ts1_datafile.dbf'
SIZE 200M AUTOEXTEND ON
EXTENT MANAGEMENT LOCAL;

-- 创建表空间 ts2
CREATE TABLESPACE ts2
DATAFILE 'ts2_datafile.dbf'
SIZE 200M AUTOEXTEND ON
EXTENT MANAGEMENT LOCAL;

```

- 表空间 ts1：
  - 用途：存储顾客表、商品表、订单表以及订单详细表等常用的表和索引。
  - 分配空间：根据预估的数据量和增长率进行分配。以下是一个示例的分配空间方案：
  - 顾客表（customer）：初始大小10MB，自动扩展
  - 商品表（products）：初始大小20MB，自动扩展
  - 订单表（orders）：初始大小30MB，自动扩展
  - 订单详细表（orderdetails）：初始大小50MB，自动扩展
- 表空间 ts2：
  - 用途：存储库存表和进货表等大型数据对象、LOB列和大文本数据。
  - 分配空间：根据预估的大型数据量和增长率进行分配。以下是一个示例的分配空间方案：
  - 库存表（stock）：初始大小20MB，自动扩展
  - 进货表（purchase）：初始大小30MB，自动扩展

![](./img/2.png)

(3)创建数据表，并根据表空间分配方案为其指定空间
- 顾客表(customer)
  - 字段有：顾客ID(customer_id), 顾客名(customer_name), 密码(customer_password), 联系方式(customer_phone), 邮箱(customer_email), 地址(customer_adress)
```
CREATE TABLE customer (
  customer_id NUMBER(9, 0) NOT NULL,
  customer_name VARCHAR2(40 BYTE) NOT NULL,
  customer_password VARCHAR2(40 BYTE),
  customer_phone VARCHAR2(40 BYTE),
  customer_email VARCHAR2(40 BYTE),
  customer_address VARCHAR2(100 BYTE),
  CONSTRAINT customer_pk PRIMARY KEY (customer_id)
)
TABLESPACE ts1;
```
![](./img/3.png)

- 商品表(products)
  - 字段有：商品ID(products_id), 商品名称(product_name), 商品单价(products_prize), 商品折扣(products_discount), 商品描述(products_dsc)
```
CREATE TABLE products (
  product_id NUMBER(9, 0) NOT NULL,
  product_name VARCHAR2(40 BYTE),
  product_price NUMBER(8, 2),
  product_discount NUMBER(8, 2),
  product_description VARCHAR2(100 BYTE),
  CONSTRAINT products_pk PRIMARY KEY (product_id)
)
TABLESPACE ts1;
```
![](./img/4.png)

- 订单表(order)
  - 字段有:订单ID(order_id), 订单日期(order_date), 订单总额(order_sum), 订单状态(order_status), 顾客名(customer_name), 顾客联系方式(customer_phone)
```
CREATE TABLE orders (
  order_id NUMBER(9, 0) NOT NULL,
  order_date DATE,
  order_sum NUMBER(8, 2),
  customer_name VARCHAR2(40 BYTE),
  customer_phone VARCHAR2(40 BYTE),
  order_status VARCHAR2(20),
  CONSTRAINT orders_pk PRIMARY KEY (order_id)
)
TABLESPACE ts1;
```

![](./img/5.png)

- 订单详表(orderdetails)
  - 字段有:订单详表ID(orderdetail_id), 订单ID(order_id), 商品ID(products_id),商品名称(product_name),商品数量(product_num), 商品价格(product_prize)
```
 CREATE TABLE orderdetails (
  orderdetail_id NUMBER(9, 0) NOT NULL,
  order_id NUMBER(9, 0),
  product_id NUMBER(9, 0),
  product_name VARCHAR2(40 BYTE),
  product_num NUMBER(8, 2),
  product_price NUMBER(8, 2),
  CONSTRAINT orderdetails_pk PRIMARY KEY (orderdetail_id),
  CONSTRAINT orderdetails_fk1 FOREIGN KEY (order_id) REFERENCES orders (order_id),
  CONSTRAINT orderdetails_fk2 FOREIGN KEY (product_id) REFERENCES products (product_id)
)
TABLESPACE ts1;
```
![](./img/6.png)

- 库存表(stock)
  - 字段有：商品ID(product_id), 库存数量(stock_num)
```
CREATE TABLE stock (
  product_id NUMBER(9, 0) NOT NULL,
  stock_num NUMBER(8, 2),
  CONSTRAINT stock_pk PRIMARY KEY (product_id),
  CONSTRAINT stock_fk1 FOREIGN KEY (product_id) REFERENCES products (product_id)
)
TABLESPACE ts2;

```
![](./img/7.png)

- 进货表(purchase)
  - 字段有：进货ID(purchase_id), 商品ID(pid), 进货单价(purchase_prize), 进货数量(purchase_num), 进货日期(purchase_date)
```
CREATE TABLE purchase (
  purchase_id NUMBER(9, 0) NOT NULL,
  product_id NUMBER(9, 0),
  purchase_price NUMBER(8, 2),
  purchase_num NUMBER(8, 2),
  purchase_date DATE,
  CONSTRAINT purchase_pk PRIMARY KEY (purchase_id),
  CONSTRAINT purchase_fk1 FOREIGN KEY (product_id) REFERENCES products (product_id)
)
TABLESPACE ts2;
```

![](./img/8.png)

(4)给每个表添加3万条数据
-- 向顾客表（customer）添加数据
```
DECLARE
  i NUMBER := 1;
BEGIN
  WHILE i <= 30000 LOOP
    INSERT INTO customer (customer_id, customer_name, customer_password, customer_phone, customer_email, customer_address)
    VALUES (i, 'Customer ' || i, 'password' || i, '123456789', 'customer' || i || '@example.com', 'Address ' || i);
    i := i + 1;
  END LOOP;
  COMMIT;
END;
```
![](./img/9.png)
![](./img/10.png)

-- 向商品表（products）添加数据
```
DECLARE
  i NUMBER := 1;
BEGIN
  WHILE i <= 30000 LOOP
    INSERT INTO products (product_id, product_name, product_price, product_discount, product_description)
    VALUES (i, 'Product ' || i, 10.99, 0.1, 'Description of Product ' || i);
    i := i + 1;
  END LOOP;
  COMMIT;
END;
```
![](./img/11.png)
![](./img/12.png)

-- 向订单表（orders）添加数据
```
DECLARE
  i NUMBER := 1;
BEGIN
  WHILE i <= 30000 LOOP
    INSERT INTO orders (order_id, order_date, order_sum, order_status,customer_name, customer_phone)
    VALUES (i, SYSDATE, 100.00,'pending', 'Customer ' || i, '123456789');
    i := i + 1;
  END LOOP;
  COMMIT;
END;
```
![](./img/13.png)
![](./img/14.png)

-- 向订单详细表（orderdetails）添加数据
```
DECLARE
  i NUMBER := 1;
BEGIN
  WHILE i <= 30000 LOOP
    INSERT INTO orderdetails (orderdetail_id, order_id, product_id, product_name, product_num, product_price)
    VALUES (i, i, i, 'Product ' || i, 2, 10.99);
    i := i + 1;
  END LOOP;
  COMMIT;
END;
```
![](./img/15.png)
![](./img/16.png)

-- 向库存表（stock）添加数据
```
DECLARE
  v_product_id NUMBER := 1;
BEGIN
  FOR i IN 1..30000 LOOP
    INSERT INTO stock (product_id, stock_num)
    VALUES (v_product_id, 100);
    
    v_product_id := v_product_id + 1;
  END LOOP;

  COMMIT;
END;

```
![](./img/17.png)
![](./img/18.png)

-- 向进货表（purchase）添加数据
```
DECLARE
  i NUMBER := 1;
  order_date DATE := DATE '1991-01-01'; -- 设置起始日期
BEGIN
  WHILE i <= 30000 LOOP
    INSERT INTO purchase (purchase_id, product_id, purchase_price, purchase_quantity, purchase_date)
    VALUES (i, i, 9.99, 50, order_date);
    i := i + 1;
    order_date := order_date + 1; -- 递增日期
  END LOOP;
  COMMIT;
END;

```
![](./img/19.png)
![](./img/20.png)

### 2.设计权限及用户分配方案,至少两个用户。
- 角色定义：
  - 管理员（hr）：具有最高权限，可以管理用户、分配权限和修改系统设置。
  - 销售员工（Salesperson）：具有销售相关权限，可以查看和编辑产品信息、处理订单等。
   -- 创建角色
(由于使用的是hr用户创建表，因此hr用户将被授予管理权限)

![](./img/21.png)
```
 CREATE ROLE salesperson;
```
![](./img/22.png)

- 权限定义
  - 管理员权限: 创建用户, 修改用户权限, 删除用户, 访问顾客表、商品表、订单表、订单详细表、库存表和进货表的所有操作权限
  - 销售员工权限：查看顾客表、商品表、订单表的内容, 创建和编辑订单, 查看库存表和进货表的内容
```
-- 管理员权限
GRANT CREATE SESSION TO hr;
GRANT CREATE USER TO hr;
GRANT ALTER USER TO hr;
GRANT DROP USER TO hr;

-- 销售员工权限
GRANT SELECT ON customers TO salesperson;
GRANT SELECT ON products TO salesperson;
GRANT SELECT ON orders TO salesperson;
GRANT INSERT, UPDATE ON orders TO salesperson;
GRANT SELECT ON inventory TO salesperson;
GRANT SELECT ON purchase TO salesperson;
```

- 用户管理
  - 创建用户：管理员可以创建新用户，并为其分配角色和权限。
  - 修改用户权限：管理员可以修改用户的角色和权限。
  - 删除用户：管理员可以删除用户及其相关信息。
```
-- 创建用户
CREATE USER sales_user IDENTIFIED BY password;

-- 分配角色
GRANT salesperson TO sales_user;
```
![](./img/23.png)

### 3.在数据库中建立一个程序包，在包中用PL/SQL语言设计一些存储过程和函数，实现比较复杂的业务逻辑。

```
-- 包规范
CREATE OR REPLACE PACKAGE sales_system_pkg AS

  -- 创建订单
  PROCEDURE create_order(
    p_order_id IN NUMBER,
    p_order_date IN DATE,
    p_customer_name IN VARCHAR2,
    p_customer_phone IN VARCHAR2,
    p_order_details IN SYS_REFCURSOR
  );
  
  -- 查询订单
  FUNCTION get_order(
    p_order_id IN NUMBER
  ) RETURN SYS_REFCURSOR;
  
  -- 添加顾客
  PROCEDURE add_customer(
    p_customer_id IN NUMBER,
    p_customer_name IN VARCHAR2,
    p_customer_password IN VARCHAR2,
    p_customer_phone IN VARCHAR2,
    p_customer_email IN VARCHAR2,
    p_customer_address IN VARCHAR2
  );
  
  -- 添加商品
  PROCEDURE add_product(
    p_product_id IN NUMBER,
    p_product_name IN VARCHAR2,
    p_product_price IN NUMBER,
    p_product_discount IN NUMBER,
    p_product_description IN VARCHAR2
  );

  -- 发货操作
  PROCEDURE ship_order(
    p_order_id IN NUMBER
  );

  -- 退货操作
  PROCEDURE return_order(
    p_order_id IN NUMBER
  );

END sales_system_pkg;
\
-- 包体
create or replace PACKAGE BODY sales_system_pkg AS

  -- 创建订单
  PROCEDURE create_order(
    p_order_id IN NUMBER,
    p_order_date IN DATE,
    p_customer_name IN VARCHAR2,
    p_customer_phone IN VARCHAR2,
    p_order_details IN SYS_REFCURSOR
  ) IS
    v_product_id NUMBER;
    v_product_name VARCHAR2(100);
    v_product_num NUMBER;
    v_product_price NUMBER;
    v_order_sum NUMBER := 0;
  BEGIN
    -- 插入订单表数据
    INSERT INTO orders (order_id, order_date, order_status, customer_name, customer_phone)
    VALUES (p_order_id, p_order_date, 'Pending', p_customer_name, p_customer_phone);

    -- 插入订单详表数据
    LOOP
      FETCH p_order_details INTO v_product_id, v_product_name, v_product_num, v_product_price;
      EXIT WHEN p_order_details%NOTFOUND;

      INSERT INTO orderdetails (orderdetail_id, order_id, product_id, product_name, product_num, product_price)
      VALUES (orderdetails_seq.NEXTVAL, p_order_id, v_product_id, v_product_name, v_product_num, v_product_price);

    -- 计算订单总额
    v_order_sum := v_order_sum + (v_product_num *v_product_price);
    END LOOP;

    -- 更新订单总额
    UPDATE orders
    SET order_sum = v_order_sum
    WHERE order_id = p_order_id;

    COMMIT;
  EXCEPTION
    WHEN OTHERS THEN
      ROLLBACK;
      RAISE;
  END create_order;

  -- 查询订单
  FUNCTION get_order(
    p_order_id IN NUMBER
  ) RETURN SYS_REFCURSOR IS
    v_result SYS_REFCURSOR;
  BEGIN
    OPEN v_result FOR
    SELECT *
    FROM orders
    WHERE order_id = p_order_id;

    RETURN v_result;
  END get_order;

  -- 添加顾客
  PROCEDURE add_customer(
    p_customer_id IN NUMBER,
    p_customer_name IN VARCHAR2,
    p_customer_password IN VARCHAR2,
    p_customer_phone IN VARCHAR2,
    p_customer_email IN VARCHAR2,
    p_customer_address IN VARCHAR2
  ) IS
  BEGIN
    INSERT INTO customer (customer_id, customer_name, customer_password, customer_phone, customer_email, customer_address)
    VALUES (p_customer_id, p_customer_name, p_customer_password, p_customer_phone, p_customer_email, p_customer_address);
    COMMIT;
  EXCEPTION
    WHEN OTHERS THEN
      ROLLBACK;
      RAISE;
  END add_customer;

  -- 添加商品
  PROCEDURE add_product(
    p_product_id IN NUMBER,
    p_product_name IN VARCHAR2,
    p_product_price IN NUMBER,
    p_product_discount IN NUMBER,
    p_product_description IN VARCHAR2
  ) IS
  BEGIN
    INSERT INTO products (product_id, product_name, product_price, product_discount, product_description)
    VALUES (p_product_id, p_product_name, p_product_price, p_product_discount, p_product_description);
    COMMIT;
  EXCEPTION
    WHEN OTHERS THEN
      ROLLBACK;
  RAISE;
  END add_product;


  -- 发货操作
  PROCEDURE ship_order(
    p_order_id IN NUMBER
  ) IS
    v_order_status orders.order_status%TYPE;
    v_product_id orderdetails.product_id%TYPE;
    v_product_num orderdetails.product_num%TYPE;
  BEGIN
    -- 检查订单状态是否为已付款
    SELECT order_status INTO v_order_status
    FROM orders
    WHERE order_id = p_order_id;

    IF v_order_status = 'Paid' THEN
      -- 修改订单状态为已发货
      UPDATE orders
      SET order_status = 'Shipped'
      WHERE order_id = p_order_id;

      -- 减少库存数量
      FOR order_detail IN (SELECT product_id, product_num
                          FROM orderdetails
                          WHERE order_id = p_order_id)
      LOOP
        v_product_id := order_detail.product_id;
        v_product_num := order_detail.product_num;

        -- 减少库存
        UPDATE stock
        SET stock_num = stock_num - v_product_num
        WHERE product_id = v_product_id;
      END LOOP;

      -- 提交事务
      COMMIT;
    ELSE
      -- 抛出异常，订单状态不正确
      RAISE_APPLICATION_ERROR(-20001, 'Order status is not valid for shipping');
    END IF;
  EXCEPTION
    WHEN NO_DATA_FOUND THEN
      -- 抛出异常，订单不存在
      RAISE_APPLICATION_ERROR(-20002, 'Order not found');
    WHEN OTHERS THEN
      ROLLBACK;
      RAISE;
  END ship_order;

  -- 退货操作
  PROCEDURE return_order(
    p_order_id IN NUMBER
  ) IS
    v_order_status orders.order_status%TYPE;
    v_product_id orderdetails.product_id%TYPE;
    v_product_num orderdetails.product_num%TYPE;
  BEGIN
    -- 检查订单状态是否为已发货
    SELECT order_status INTO v_order_status
    FROM orders
    WHERE order_id = p_order_id;

    IF v_order_status = 'Shipped' THEN
      -- 修改订单状态为已退货
      UPDATE orders
      SET order_status = 'Returned'
      WHERE order_id = p_order_id;

      -- 增加库存数量
      FOR order_detail IN (SELECT product_id, product_num
                          FROM orderdetails
                          WHERE order_id = p_order_id)
      LOOP
        v_product_id := order_detail.product_id;
        v_product_num := order_detail.product_num;

        -- 增加库存
        UPDATE stock
        SET stock_num = stock_num + v_product_num
        WHERE product_id = v_product_id;
      END LOOP;

      -- 提交事务
      COMMIT;
    ELSE
      -- 抛出异常，订单状态不正确
      RAISE_APPLICATION_ERROR(-20003, 'Order status is not valid for return');
    END IF;
  EXCEPTION
    WHEN NO_DATA_FOUND THEN
      -- 抛出异常，订单不存在
      RAISE_APPLICATION_ERROR(-20004, 'Order not found');
    WHEN OTHERS THEN
      ROLLBACK;
      RAISE;
  END return_order;

END sales_system_pkg;
```
![](./img/24.png)
程序包已经正确编译
![](./img/25.png)
### 4.设计一套数据库的备份方案
(1)备份策略：
- 完全备份：每周进行一次完全备份，备份整个数据库。
- 增量备份：每天进行增量备份，备份最近一次完全备份之后的数据变化。
- 归档日志备份：定期备份数据库的归档日志，以确保日志链的完整性
- 同时，对于重要数据的表，可以考虑每小时进行一次日志备份。

(2)备份工具：
- Oracle Recovery Manager (RMAN)：使用 RMAN 工具进行备份和恢复操作，利用其自动备份和恢复功能，简化备份流程。
- 数据库热备份：使用 Oracle 数据库的在线备份功能，允许在备份过程中继续对数据库进行读写操作。

(3)备份方式：
- 本地备份：在备份服务器上设置备份目录，将备份数据直接保存在备份目录中。这种方式备份速度快，但是如果备份服务器遭到损坏或者故障，备份数据也会受到影响。
- 远程备份：备份数据保存在远程服务器上，可以提高备份数据的安全性。建议使用专门的远程备份服务器进行备份数据的存储。
- 云备份：将备份数据保存在云端，可以提高备份数据的安全性和可靠性。但是，需要注意的是，云备份的速度可能会受到网络带宽的影响。

(3)备份类型：
 - 完全备份：使用 RMAN 进行完全备份，备份整个数据库的数据文件、控制文件和日志文件。
 - 增量备份：使用 RMAN 进行增量备份，备份最近一次完全备份之后的数据变化。
- 归档日志备份：
- 备份数据库的归档日志，确保在灾难恢复时能够还原到指定的时间点。

(4)备份验证
- 恢复备份数据:将备份数据恢复到一个测试环境或者独立的数据库实例中，确保数据可以成功恢复，并且与原始数据库的数据一致。
- 验证数据完整性:对恢复的数据进行完整性校验，比对备份前后的数据是否一致，可以通过对比表记录数量、关键字段的值、索引结构等方式进行验证。
- 执行功能测试:在恢复的测试环境中，执行一些常见的功能测试，确保系统在备份数据恢复后能够正常运行，例如验证商品销售、订单处理、库存管理等核心功能是否正常工作。
- 性能测试:对恢复的测试环境进行性能测试，模拟实际使用场景，确保备份数据的恢复不会对系统性能产生不良影响。

(5)定期备份计划：
 - 完全备份：每周一次，在非高峰期进行完全备份。
 - 增量备份：每天进行一次增量备份，以捕捉最新的数据变化。
归档日志备份：根据归档日志生成的频率和大小，定期备份归档日志，以确保日志链的完整性。

(6)备份存储管理：
 - 将备份数据存储在磁盘或移动硬盘上，设置合适的目录结构和命名约定，以便管理和检索备份文件。
 - 考虑数据保留期限和存储容量，制定备份数据的保留策略和清理机制，避免存储空间不足或过期数据的累积。

(7)数据安全：
- 数据加密：使用 RMAN 提供的加密功能对备份数据进行加密，保护备份数据的机密性。
- 访问控制：限制对备份数据的访问权限，只授权给需要的管理员或备份操作人员。
 - 安全传输：如果备份数据需要在网络上传输，使用安全通信协

(8)监控与日志记录：
- 监控备份过程：实时监控备份任务的执行状态和进度，确保备份任务按计划进行。
- 记录备份日志：记录每次备份操作的详细信息，包括备份开始时间、结束时间、备份文件路径等，以便日后审计和故障排查。

(9)容灾备份：
- 跨地理位置备份：在地理上分离备份数据的存储位置，避免单点故障和自然灾害对备份数据的影响。
- 离线存储备份：将部分备份数据存储在离线介质上，如磁带，以提供离线备份和长期存档的能力。

(10)恢复测试：
- 定期进行恢复测试：定期验证备份数据的可用性和恢复过程的有效性，确保备份的可靠性和完整性。
- 模拟灾难恢复：模拟真实的灾难情景，测试灾难恢复计划和流程，以确保能够快速、正确地恢复系统。

(11)自动化备份：
- 自动备份脚本：编写自动化脚本，包括备份计划的配置、备份任务的触发和执行，以减少人工操作和人为错误的风险。
- 定时任务调度：使用操作系统的定时任务调度工具，如crontab（Linux/Unix）或任务计划（Windows），自动触发备份任务的执行。

(12)定期检查和维护：
- 检查备份数据完整性：定期验证备份文件的完整性和一致性，以确保备份数据没有损坏或丢失。
- 清理过期备份：根据备份数据的保留策略，定期清理过期的备份文件，释放存储空间并保持备份环境的良好状态。

(13)灾难恢复计划：
- 定义灾难恢复策略：制定灾难恢复计划，包括灾难恢复的优先级、步骤和所需资源，确保在灾难事件发生时能够快速有效地恢复系统。
- 文档化恢复过程：详细记录灾难恢复的步骤和操作指南，包括所需的备份文件、命令和配置信息，以便在紧急情况下参考和操作。

## 总结和体会
在完成基于Oracle数据库的商品销售系统数据库设计时，我深入学习了Oracle数据库的各个方面，并应用这些知识进行了实际设计。整个过程涉及到表和表空间设计、权限和用户分配、存储过程和函数设计以及备份方案。通过这个项目，我对Oracle数据库有了更全面的了解，并掌握了如何设计和管理一个复杂的数据库系统。
- 表和表空间设计方案：
  - 在设计表和表空间时，我考虑了商品销售系统的需求，并选择了适当的数据结构和关系。我创建了以下几张表：
    - 顾客表：记录客户的信息，包括顾客ID、姓名、联系方式等。
    - 商品表：包含商品的基本信息，如商品ID、名称、价格等。
    - 订单表和订单详表：用于存储客户的订单信息，包括订单ID、客户ID、商品ID、数量、总价等。
    - 库存表：跟踪商品的库存情况，包括商品ID、库存数量等。
    - 进货表：了解商品的进货渠道和利润等，包括进价，数量等。
  - 我还设计了至少两个表空间，以实现数据的分离和管理。
- 权限和用户分配方案：
  - 为了确保数据的安全性和访问控制，我设计了不同的用户和角色，并分配了相应的权限。至少有两个用户：
    - 管理员用户：具有最高权限，可以创建表、用户和角色，管理表空间，以及执行任何数据库操作。
    - 销售员用户：具有较低权限，可以查询和修改商品信息，处理客户订单等。
- 存储过程和函数设计：
  - 为了实现复杂的业务逻辑，我在数据库中创建了一个程序包，并使用PL/SQL语言设计了一些存储过程和函数。这些存储过程和函数可以用于处理订单、更新库存、计算销售统计数据等。例如，我设计了一个存储过程用于创建新订单，其中包含了对订单表和库存表的更新操作。
- 数据库备份方案：
  - 为了保护数据库中的数据免受意外损坏或丢失，我设计了一套数据库备份方案。我定期进行完整备份，并使用增量备份策略来减少备份时间和存储空间。此外，我还设置了自动备份任务，以确保备份的及时性和一致性。

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;通过完成这个系统的数据库设计，我学到了很多关于Oracle数据库的知识和技能。我理解了数据库设计的重要性，以及如何根据需求选择合适的数据结构和关系。我还学习了如何创建用户和角色，并分配适当的权限以实现安全访问控制。此外，我通过编写存储过程和函数，学会了如何利用PL/SQL语言实现复杂的业务逻辑和数据处理。<p>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;在备份方面，我了解了备份策略的选择和实施。我学习使用Oracle提供的备份工具和功能，如RMAN（Recovery Manager），以及如何设置定期备份和恢复策略来确保数据的完整性和可用性。<p>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;在整个过程中，我还遇到了一些挑战。例如，设计表结构时需要考虑到数据的一致性和性能方面的问题。我学习了如何进行合理的索引设计和规范化处理，以提高查询效率和数据存储优化。此外，掌握PL/SQL语言和编写存储过程的技能对于实现复杂的业务逻辑非常关键。我通过学习和实践，逐渐掌握了这些技能并提升了我的数据库设计和开发能力。<p>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;总体而言，完成基于Oracle数据库的商品销售系统的数据库设计是一个富有挑战性和收获的过程。通过学习Oracle数据库的各个方面，我不仅掌握了数据库设计和管理的基本原理和技能，还了解了如何应用这些知识来构建一个高效、安全和可靠的数据库系统。这个项目使我在Oracle学习的过程中获得了宝贵的实践经验，并提升了我的专业能力和技术水平。我期待在未来的项目开发和编程实践中可以利用我在这次实验中所学到的一些设计思想和方案。<p>
