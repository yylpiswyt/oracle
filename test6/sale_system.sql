-- 创建表空间 ts1
CREATE TABLESPACE ts1
DATAFILE 'ts1_datafile.dbf'
SIZE 200M AUTOEXTEND ON
EXTENT MANAGEMENT LOCAL;

-- 创建表空间 ts2
CREATE TABLESPACE ts2
DATAFILE 'ts2_datafile.dbf'
SIZE 200M AUTOEXTEND ON
EXTENT MANAGEMENT LOCAL;

-- 创建顾客表
CREATE TABLE customer (
  customer_id NUMBER(9, 0) NOT NULL,
  customer_name VARCHAR2(40 BYTE) NOT NULL,
  customer_password VARCHAR2(40 BYTE),
  customer_phone VARCHAR2(40 BYTE),
  customer_email VARCHAR2(40 BYTE),
  customer_address VARCHAR2(100 BYTE),
  CONSTRAINT customer_pk PRIMARY KEY (customer_id)
)
TABLESPACE ts1;

-- 创建商品表
CREATE TABLE products (
  product_id NUMBER(9, 0) NOT NULL,
  product_name VARCHAR2(40 BYTE),
  product_price NUMBER(8, 2),
  product_discount NUMBER(8, 2),
  product_description VARCHAR2(100 BYTE),
  CONSTRAINT products_pk PRIMARY KEY (product_id)
)
TABLESPACE ts1;

-- 创建订单表
CREATE TABLE orders (
  order_id NUMBER(9, 0) NOT NULL,
  order_date DATE,
  order_sum NUMBER(8, 2),
  customer_name VARCHAR2(40 BYTE),
  customer_phone VARCHAR2(40 BYTE),
  order_status VARCHAR2(20),
  CONSTRAINT orders_pk PRIMARY KEY (order_id)
)
TABLESPACE ts1;

-- 创建订单详表
 CREATE TABLE orderdetails (
  orderdetail_id NUMBER(9, 0) NOT NULL,
  order_id NUMBER(9, 0),
  product_id NUMBER(9, 0),
  product_name VARCHAR2(40 BYTE),
  product_num NUMBER(8, 2),
  product_price NUMBER(8, 2),
  CONSTRAINT orderdetails_pk PRIMARY KEY (orderdetail_id),
  CONSTRAINT orderdetails_fk1 FOREIGN KEY (order_id) REFERENCES orders (order_id),
  CONSTRAINT orderdetails_fk2 FOREIGN KEY (product_id) REFERENCES products (product_id)
)
TABLESPACE ts1;

-- 创建库存表
CREATE TABLE stock (
  product_id NUMBER(9, 0) NOT NULL,
  stock_num NUMBER(8, 2),
  CONSTRAINT stock_pk PRIMARY KEY (product_id),
  CONSTRAINT stock_fk1 FOREIGN KEY (product_id) REFERENCES products (product_id)
)
TABLESPACE ts2;

-- 创建进货表
CREATE TABLE purchase (
  purchase_id NUMBER(9, 0) NOT NULL,
  product_id NUMBER(9, 0),
  purchase_price NUMBER(8, 2),
  purchase_num NUMBER(8, 2),
  purchase_date DATE,
  CONSTRAINT purchase_pk PRIMARY KEY (purchase_id),
  CONSTRAINT purchase_fk1 FOREIGN KEY (product_id) REFERENCES products (product_id)
)
TABLESPACE ts2;

-- 向顾客表（customer）添加数据
DECLARE
  i NUMBER := 1;
BEGIN
  WHILE i <= 30000 LOOP
    INSERT INTO customer (customer_id, customer_name, customer_password, customer_phone, customer_email, customer_address)
    VALUES (i, 'Customer ' || i, 'password' || i, '123456789', 'customer' || i || '@example.com', 'Address ' || i);
    i := i + 1;
  END LOOP;
  COMMIT;
END;

-- 向商品表（products）添加数据
DECLARE
  i NUMBER := 1;
BEGIN
  WHILE i <= 30000 LOOP
    INSERT INTO products (product_id, product_name, product_price, product_discount, product_description)
    VALUES (i, 'Product ' || i, 10.99, 0.1, 'Description of Product ' || i);
    i := i + 1;
  END LOOP;
  COMMIT;
END;

-- 向订单表（orders）添加数据
DECLARE
  i NUMBER := 1;
BEGIN
  WHILE i <= 30000 LOOP
    INSERT INTO orders (order_id, order_date, order_sum, order_status,customer_name, customer_phone)
    VALUES (i, SYSDATE, 100.00,'pending', 'Customer ' || i, '123456789');
    i := i + 1;
  END LOOP;
  COMMIT;
END;

-- 向订单详细表（orderdetails）添加数据
DECLARE
  i NUMBER := 1;
BEGIN
  WHILE i <= 30000 LOOP
    INSERT INTO orderdetails (orderdetail_id, order_id, product_id, product_name, product_num, product_price)
    VALUES (i, i, i, 'Product ' || i, 2, 10.99);
    i := i + 1;
  END LOOP;
  COMMIT;
END;

-- 向库存表（stock）添加数据
DECLARE
  v_product_id NUMBER := 1;
BEGIN
  FOR i IN 1..30000 LOOP
    INSERT INTO stock (product_id, stock_num)
    VALUES (v_product_id, 100);
    
    v_product_id := v_product_id + 1;
  END LOOP;

  COMMIT;
END;


-- 向进货表（purchase）添加数据
DECLARE
  i NUMBER := 1;
  order_date DATE := DATE '1991-01-01'; -- 设置起始日期
BEGIN
  WHILE i <= 30000 LOOP
    INSERT INTO purchase (purchase_id, product_id, purchase_price, purchase_quantity, purchase_date)
    VALUES (i, i, 9.99, 50, order_date);
    i := i + 1;
    order_date := order_date + 1; -- 递增日期
  END LOOP;
  COMMIT;
END;

-- 包规范
CREATE OR REPLACE PACKAGE sales_system_pkg AS

  -- 创建订单
  PROCEDURE create_order(
    p_order_id IN NUMBER,
    p_order_date IN DATE,
    p_customer_name IN VARCHAR2,
    p_customer_phone IN VARCHAR2,
    p_order_details IN SYS_REFCURSOR
  );
  
  -- 查询订单
  FUNCTION get_order(
    p_order_id IN NUMBER
  ) RETURN SYS_REFCURSOR;
  
  -- 添加顾客
  PROCEDURE add_customer(
    p_customer_id IN NUMBER,
    p_customer_name IN VARCHAR2,
    p_customer_password IN VARCHAR2,
    p_customer_phone IN VARCHAR2,
    p_customer_email IN VARCHAR2,
    p_customer_address IN VARCHAR2
  );
  
  -- 添加商品
  PROCEDURE add_product(
    p_product_id IN NUMBER,
    p_product_name IN VARCHAR2,
    p_product_price IN NUMBER,
    p_product_discount IN NUMBER,
    p_product_description IN VARCHAR2
  );

  -- 发货操作
  PROCEDURE ship_order(
    p_order_id IN NUMBER
  );

  -- 退货操作
  PROCEDURE return_order(
    p_order_id IN NUMBER
  );

END sales_system_pkg;
\
-- 包体
create or replace PACKAGE BODY sales_system_pkg AS

  -- 创建订单
  PROCEDURE create_order(
    p_order_id IN NUMBER,
    p_order_date IN DATE,
    p_customer_name IN VARCHAR2,
    p_customer_phone IN VARCHAR2,
    p_order_details IN SYS_REFCURSOR
  ) IS
    v_product_id NUMBER;
    v_product_name VARCHAR2(100);
    v_product_num NUMBER;
    v_product_price NUMBER;
    v_order_sum NUMBER := 0;
  BEGIN
    -- 插入订单表数据
    INSERT INTO orders (order_id, order_date, order_status, customer_name, customer_phone)
    VALUES (p_order_id, p_order_date, 'Pending', p_customer_name, p_customer_phone);

    -- 插入订单详表数据
    LOOP
      FETCH p_order_details INTO v_product_id, v_product_name, v_product_num, v_product_price;
      EXIT WHEN p_order_details%NOTFOUND;

      INSERT INTO orderdetails (orderdetail_id, order_id, product_id, product_name, product_num, product_price)
      VALUES (orderdetails_seq.NEXTVAL, p_order_id, v_product_id, v_product_name, v_product_num, v_product_price);

    -- 计算订单总额
    v_order_sum := v_order_sum + (v_product_num *v_product_price);
    END LOOP;

    -- 更新订单总额
    UPDATE orders
    SET order_sum = v_order_sum
    WHERE order_id = p_order_id;

    COMMIT;
  EXCEPTION
    WHEN OTHERS THEN
      ROLLBACK;
      RAISE;
  END create_order;

  -- 查询订单
  FUNCTION get_order(
    p_order_id IN NUMBER
  ) RETURN SYS_REFCURSOR IS
    v_result SYS_REFCURSOR;
  BEGIN
    OPEN v_result FOR
    SELECT *
    FROM orders
    WHERE order_id = p_order_id;

    RETURN v_result;
  END get_order;

  -- 添加顾客
  PROCEDURE add_customer(
    p_customer_id IN NUMBER,
    p_customer_name IN VARCHAR2,
    p_customer_password IN VARCHAR2,
    p_customer_phone IN VARCHAR2,
    p_customer_email IN VARCHAR2,
    p_customer_address IN VARCHAR2
  ) IS
  BEGIN
    INSERT INTO customer (customer_id, customer_name, customer_password, customer_phone, customer_email, customer_address)
    VALUES (p_customer_id, p_customer_name, p_customer_password, p_customer_phone, p_customer_email, p_customer_address);
    COMMIT;
  EXCEPTION
    WHEN OTHERS THEN
      ROLLBACK;
      RAISE;
  END add_customer;

  -- 添加商品
  PROCEDURE add_product(
    p_product_id IN NUMBER,
    p_product_name IN VARCHAR2,
    p_product_price IN NUMBER,
    p_product_discount IN NUMBER,
    p_product_description IN VARCHAR2
  ) IS
  BEGIN
    INSERT INTO products (product_id, product_name, product_price, product_discount, product_description)
    VALUES (p_product_id, p_product_name, p_product_price, p_product_discount, p_product_description);
    COMMIT;
  EXCEPTION
    WHEN OTHERS THEN
      ROLLBACK;
  RAISE;
  END add_product;


  -- 发货操作
  PROCEDURE ship_order(
    p_order_id IN NUMBER
  ) IS
    v_order_status orders.order_status%TYPE;
    v_product_id orderdetails.product_id%TYPE;
    v_product_num orderdetails.product_num%TYPE;
  BEGIN
    -- 检查订单状态是否为已付款
    SELECT order_status INTO v_order_status
    FROM orders
    WHERE order_id = p_order_id;

    IF v_order_status = 'Paid' THEN
      -- 修改订单状态为已发货
      UPDATE orders
      SET order_status = 'Shipped'
      WHERE order_id = p_order_id;

      -- 减少库存数量
      FOR order_detail IN (SELECT product_id, product_num
                          FROM orderdetails
                          WHERE order_id = p_order_id)
      LOOP
        v_product_id := order_detail.product_id;
        v_product_num := order_detail.product_num;

        -- 减少库存
        UPDATE stock
        SET stock_num = stock_num - v_product_num
        WHERE product_id = v_product_id;
      END LOOP;

      -- 提交事务
      COMMIT;
    ELSE
      -- 抛出异常，订单状态不正确
      RAISE_APPLICATION_ERROR(-20001, 'Order status is not valid for shipping');
    END IF;
  EXCEPTION
    WHEN NO_DATA_FOUND THEN
      -- 抛出异常，订单不存在
      RAISE_APPLICATION_ERROR(-20002, 'Order not found');
    WHEN OTHERS THEN
      ROLLBACK;
      RAISE;
  END ship_order;

  -- 退货操作
  PROCEDURE return_order(
    p_order_id IN NUMBER
  ) IS
    v_order_status orders.order_status%TYPE;
    v_product_id orderdetails.product_id%TYPE;
    v_product_num orderdetails.product_num%TYPE;
  BEGIN
    -- 检查订单状态是否为已发货
    SELECT order_status INTO v_order_status
    FROM orders
    WHERE order_id = p_order_id;

    IF v_order_status = 'Shipped' THEN
      -- 修改订单状态为已退货
      UPDATE orders
      SET order_status = 'Returned'
      WHERE order_id = p_order_id;

      -- 增加库存数量
      FOR order_detail IN (SELECT product_id, product_num
                          FROM orderdetails
                          WHERE order_id = p_order_id)
      LOOP
        v_product_id := order_detail.product_id;
        v_product_num := order_detail.product_num;

        -- 增加库存
        UPDATE stock
        SET stock_num = stock_num + v_product_num
        WHERE product_id = v_product_id;
      END LOOP;

      -- 提交事务
      COMMIT;
    ELSE
      -- 抛出异常，订单状态不正确
      RAISE_APPLICATION_ERROR(-20003, 'Order status is not valid for return');
    END IF;
  EXCEPTION
    WHEN NO_DATA_FOUND THEN
      -- 抛出异常，订单不存在
      RAISE_APPLICATION_ERROR(-20004, 'Order not found');
    WHEN OTHERS THEN
      ROLLBACK;
      RAISE;
  END return_order;

END sales_system_pkg;



