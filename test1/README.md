# 班级：20级软工1班 学号：202010424122 姓名：王玉婷

# 实验1：SQL语句的执行计划分析与优化指导

# 实验目的

  分析SQL执行计划，执行SQL语句的优化指导。理解分析SQL语句的执行计划的重要作用。

# 实验数据库和用户

  数据库是pdborcl，用户是sys和hr

# 实验内容

- 对Oracle12c中的HR人力资源管理系统中的表进行查询与分析。<p>
- 设计自己的查询语句，并作相应的分析，查询语句不能太简单。执行两个比较复杂的返回相同查询结果数据集的SQL语句，通过分析SQL语句各自的执行计划，判断哪个SQL语句是最优的。最后将你认为最优的SQL语句通过sqldeveloper的优化指导工具进行优化指导，看看该工具有没有给出优化建议。<p>


# 实验过程

## 1.向用户hr授予以下视图的选择权限
$ sqlplus sys/123@localhost/pdborcl as sysdba<p>
@$ORACLE_HOME/sqlplus/admin/plustrce.sql<p>
create role plustrace;<p>
GRANT SELECT ON v_$sesstat TO plustrace;<p>
GRANT SELECT ON v_$statname TO plustrace;<p>
GRANT SELECT ON v_$mystat TO plustrace;<p>
GRANT plustrace TO dba WITH ADMIN OPTION;<p>
GRANT plustrace TO hr;<p>
GRANT SELECT ON v_$sql TO hr;<p>
GRANT SELECT ON v_$sql_plan TO hr;<p>
GRANT SELECT ON v_$sql_plan_statistics_all TO hr;<p>
GRANT SELECT ON v_$session TO hr;<p>
GRANT SELECT ON v_$parameter TO hr; <p>
![](./p1.png)

## 2.执行查询语句对Oracle12c中的HR人力资源管理系统中的表进行查询
### 查询1：<p>
$sqlplus hr/123@localhost/pdborcl<p>
<p>
set autotrace on<p>
<p>
SELECT d.department_name,count(e.job_id)as "部门总人数",<p>
avg(e.salary)as "平均工资"<p>
from hr.departments d,hr.employees e<p>
where d.department_id = e.department_id<p>
and d.department_name in ('IT','Sales')<p>
GROUP BY d.department_name;<p>
<p>
#### 输出结果：<p>
DEPARTMENT_NAME                部门总人数   平均工资<p>
------------------------------ ---------- ----------<p>
IT					5	5760<p>
Sales				       34 8955.88235<p>
<p>
<p>
执行计划<p>
----------------------------------------------------------<p>
Plan hash value: 3808327043<p>
<p>
Predicate Information (identified by operation id):<p>
---------------------------------------------------<p>
<p>
   4 - filter("D"."DEPARTMENT_NAME"='IT' OR "D"."DEPARTMENT_NAME"='Sales')<p>
   5 - access("D"."DEPARTMENT_ID"="E"."DEPARTMENT_ID")<p>
<p>
Note<p>
-----<p>
   - this is an adaptive plan<p>
<p>
统计信息<p>
----------------------------------------------------------<p>
	239  recursive calls<p>
	 13  db block gets<p>
	452  consistent gets<p>
	 14  physical reads<p>
       2144  redo size<p>
	815  bytes sent via SQL*Net to client<p>
	608  bytes received via SQL*Net from client<p>
	  2  SQL*Net roundtrips to/from client<p>
	 19  sorts (memory)<p>
	  0  sorts (disk)<p>
	  2  rows processed<p>
![](./p2.jpg)

### 查询2：<p>
set autotrace on<p>
<p>
SELECT d.department_name,count(e.job_id)as "部门总人数",<p>
avg(e.salary)as "平均工资"<p>
FROM hr.departments d,hr.employees e<p>
WHERE d.department_id = e.department_id<p>
GROUP BY d.department_name<p>
HAVING d.department_name in ('IT','Sales');<p>
<p>
#### 输出结果：<p>
------------------------------ ---------- ----------<p>
IT					5	5760<p>
Sales				       34 8955.88235<p>
<p>
<p>
执行计划<p>
----------------------------------------------------------<p>
Plan hash value: 2128232041<p>
<p>
<p>
Predicate Information (identified by operation id):<p>
---------------------------------------------------<p>
<p>
   1 - filter("D"."DEPARTMENT_NAME"='IT' OR "D"."DEPARTMENT_NAME"='Sales')<p>
   6 - access("D"."DEPARTMENT_ID"="E"."DEPARTMENT_ID")<p>
       filter("D"."DEPARTMENT_ID"="E"."DEPARTMENT_ID")<p>
<p>
统计信息<p>
----------------------------------------------------------<p>
	214  recursive calls<p>
	  0  db block gets<p>
	306  consistent gets<p>
	  6  physical reads<p>
	  0  redo size<p>
	815  bytes sent via SQL*Net to client<p>
	608  bytes received via SQL*Net from client<p>
	  2  SQL*Net roundtrips to/from client<p>
	 16  sorts (memory)<p>
	  0  sorts (disk)<p>
	  2  rows processed<p>
![](./p3.png)

## 3.通过sqldeveloper的优化指导工具进行优化指导
![](./p4.png)


## 4.分析
这两个SQL语句都是查询部门名为 "IT" 和 "Sales" 的员工数量和平均工资。但是它们之间存在一个重要的区别，就是第二个语句使用了HAVING子句，而第一个语句使用了WHERE子句来筛选数据。具体来说，HAVING子句在GROUP BY之后执行筛选，而WHERE子句在GROUP BY之前执行筛选。<p>
第二个SQL查询语句的执行计划相对简单，没有使用连接操作，而是直接使用了WHERE 子句进行条件限制，因此在性能方面更好。<p>
同时我也可以根据执行输出的统计信息来进行判断，<p>
    recursive calls递归调用（函数或过程自身调用自身的次数）<p>
    db block gets数据库块读取（从磁盘读取数据块的次数）<p>
    consistent gets一致性读取（从缓存中读取数据块的次数）<p>
    physical reads物理读取（从磁盘读取数据块的次数）<p>
    redo size字节重做（事务日志中记录的字节数）<p>
    sorts (memory)内存排序（在内存中进行的排序次数）<p>
通过比较这些数据可以发现第二个sql语句更好。<p>