# 班级：20级软工1班 学号：202010424122 姓名：王玉婷
# 实验2：用户及权限管理

## 实验目的

掌握用户管理、角色管理、权根维护与分配的能力，掌握用户之间共享对象的操作技能，以及概要文件对用户的限制。

## 实验内容

Oracle有一个开发者角色resource，可以创建表、过程、触发器等对象，但是不能创建视图。<p>

1.在pdborcl插接式数据中创建一个新的本地角色wyt_role，该角色包含connect和resource角色，同时也包含CREATE VIEW权限，这样任何拥有wyt_role的用户就同时拥有这三种权限。<p>
2.创建角色之后，再创建用户sale，给用户分配表空间，设置限额为50M，授予wyt_role角色。<p>
3.最后测试：用新用户sale连接数据库、创建表，插入数据，创建视图，查询表和视图的数据。<p>

## 实验步骤

### 1.向用户hr授予以下视图的选择权限
$ sqlplus system/123@pdborcl
CREATE ROLE wyt_role;
GRANT connect,resource,CREATE VIEW TO con_res_role;
CREATE USER sale IDENTIFIED BY 123 DEFAULT TABLESPACE users TEMPORARY TABLESPACE temp;
ALTER USER sale default TABLESPACE "USERS";
ALTER USER sale QUOTA 50M ON users;
GRANT con_res_role TO sale;
<p>

![](1.jpg)

### 2.新用户sale连接到pdborcl，创建表customers和视图customers_view，插入数据，最后将customers_view的SELECT对象权限授予hr用户。session_privs，session_roles可以查看会话权限和角色。<p>

$ sqlplus sale/123@pdborcl
SQL> show user;
USER is "sale"
SQL>
SELECT * FROM session_privs;
SELECT * FROM session_roles;
SQL>
CREATE TABLE customers (id number,name varchar(50)) TABLESPACE "USERS" ;
INSERT INTO customers(id,name)VALUES(1,'zhang');
INSERT INTO customers(id,name)VALUES (2,'wang');
CREATE VIEW customers_view AS SELECT name FROM customers;
GRANT SELECT ON customers_view TO hr;
SELECT * FROM customers_view;
<p>

![](2.jpg)


### 3.用户hr连接到pdborcl，查询sale授予它的视图customers_view
$ sqlplus hr/123@pdborcl
SQL> SELECT * FROM sale.customers;
SQL> SELECT * FROM sale.customers_view;

![](3.png)
<p>
可以看到hr用户查询customers表查询不到结果，只能查询视图customers_view，因为sale用户没有共享表customers给hr用户。

### 4.概要文件设置,用户最多登录时最多只能错误3次
$ sqlplus system/123@pdborcl
SQL>ALTER PROFILE default LIMIT FAILED_LOGIN_ATTEMPTS 3;

![](4.png)

设置后，sqlplus hr/错误密码@pdborcl。3次错误密码后，用户被锁定。<p>
![](5.png)

锁定后，通过system用户登录，alter user sale unlock命令解锁。<p>
![](6.png)

$ sqlplus system/123@pdborcl
SQL> alter user sale  account unlock;
解锁后可以登录hr用户了<p>
![](7.png)

### 5. 数据库和表空间占用分析
 (1)查看数据库的使用情况<P>
$ sqlplus system/123@pdborcl

SQL>SELECT tablespace_name,FILE_NAME,BYTES/1024/1024 MB,MAXBYTES/1024/1024 MAX_MB,autoextensible FROM dba_data_files  WHERE  tablespace_name='USERS';<p>
![](8.png)
<p>
(2)查看空间占用情况<p>
SQL>SELECT a.tablespace_name "表空间名",Total/1024/1024 "大小MB",
 free/1024/1024 "剩余MB",( total - free )/1024/1024 "使用MB",
 Round(( total - free )/ total,4)* 100 "使用率%"
 from (SELECT tablespace_name,Sum(bytes)free
        FROM   dba_free_space group  BY tablespace_name)a,
       (SELECT tablespace_name,Sum(bytes)total FROM dba_data_files
        group  BY tablespace_name)b
 where  a.tablespace_name = b.tablespace_name;

![](9.png)


### 6.实验总结
本次实验让我更深刻地认识到了Oracle数据库中用户和权限管理的重要性，以及其在应用系统中的作用。<p>通过合理地分配用户和角色的权限，可以实现对数据库的安全管理和资源控制，保障了数据库的完整性和可用性。
<p>
在实验过程中，我学习了如何创建角色、用户，并授权不同的权限。通过角色的授予和收回，可以灵活地管理用户的权限，有效地保护了数据库的安全。<p>同时，也了解了表空间的概念和使用方法，以及限额的设置和管理。这些知识和技能在实际的数据库管理和应用开发中都具有重要的作用。
