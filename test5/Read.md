# 班级：20级软工1班 学号：202010424122 姓名：王玉婷
# 实验5：包，过程，函数的用法

## 实验目的

- 了解PL/SQL语言结构
- 了解PL/SQL变量和常量的声明和使用方法
- 学习包，过程，函数的用法。

## 实验内容

- 以hr用户登录

1. 创建一个包(Package)，包名是MyPack。
2. 在MyPack中创建一个函数Get_SalaryAmount,输入的参数是部门ID，通过查询员工表，统计每个部门的salay工资总额。
3. 在MyPack中创建一个过程GET_EMPLOYEES,输入参数是员工ID，在过程中使用游标，通过查询员工表，递归查询某个员工及其所有下属，子下属员工。
Oracle递归查询的语句格式是：

```sql
SELECT LEVEL,EMPLOYEE_ID,FIRST_NAME,MANAGER_ID FROM employees 
START WITH EMPLOYEE_ID = V_EMPLOYEE_ID 
CONNECT BY PRIOR EMPLOYEE_ID = MANAGER_ID
```
## 实验步骤

### 1.创建一个包(Package)，包名是MyPack

```
CREATE OR REPLACE PACKAGE MyPack AS
  FUNCTION Get_SalaryAmount(p_department_id IN NUMBER) RETURN NUMBER;
  PROCEDURE GET_EMPLOYEES(p_employee_id IN NUMBER);
END MyPack;
```
![](1.png)
### 2.在MyPack中创建一个函数Get_SalaryAmount,输入的参数是部门ID，通过查询员工表，统计每个部门的salay工资总额
```
CREATE OR REPLACE PACKAGE BODY MyPack AS
  FUNCTION Get_SalaryAmount(p_department_id IN NUMBER) RETURN NUMBER IS
    v_salary_total NUMBER := 0;
  BEGIN
    SELECT SUM(salary) INTO v_salary_total
    FROM employees
    WHERE department_id = p_department_id;
    
    RETURN v_salary_total;
  END Get_SalaryAmount;
  
END MyPack;
```
### 3.在MyPack中创建一个过程GET_EMPLOYEES,输入参数是员工ID，在过程中使用游标，通过查询员工表，递归查询某个员工及其所有下属，子下属员工
```
 PROCEDURE GET_EMPLOYEES(p_employee_id IN NUMBER) IS
    CURSOR c_emp IS
      SELECT LEVEL, employee_id, first_name, manager_id
      FROM employees
      START WITH employee_id = p_employee_id
      CONNECT BY PRIOR employee_id = manager_id;
    v_level NUMBER;
    v_employee_id NUMBER;
    v_first_name VARCHAR2(50);
    v_manager_id NUMBER;
  BEGIN
    OPEN c_emp;
    LOOP
      FETCH c_emp INTO v_level, v_employee_id, v_first_name, v_manager_id;
      EXIT WHEN c_emp%NOTFOUND;
      
      FOR i IN 1..v_level LOOP
        DBMS_OUTPUT.PUT('  ');
      END LOOP;
      
      DBMS_OUTPUT.PUT_LINE(v_first_name);
    END LOOP;
    CLOSE c_emp;
  END GET_EMPLOYEES;
  ```
![](2.png)
  ### 4.测试函数Get_SalaryAmount和存储过程GET_EMPLOYEES

测试函数Get_SalaryAmount:
```
select department_id,department_name,MyPack.Get_SalaryAmount(department_id) AS salary_total from departments;

输出结果：
DEPARTMENT_ID DEPARTMENT_NAME                SALARY_TOTAL
------------- ------------------------------ ------------
           10 Administration                           4442
           20 Marketing                              19084
           30 Purchasing                             25152
           40 Human Resources                       6542
           50 Shipping                              158290
           60 IT                                    29010
           70 Public Relations                         10042
           80 Sales                                305928
           90 Executive                              58126
          100 Finance                               51860
          110 Accounting                             20392
```
![](3.png)
测试存储过程GET_EMPLOYEES：
```
set serveroutput on
DECLARE
p_employee_id NUMBER;    
BEGIN
p_employee_id := 101;
MYPACK.Get_Employees (  p_employee_id => p_employee_id) ;    
END;

输出结果：
Neena
    Nancy
      Daniel
      John
      Ismael
      Jose Manuel
      Luis
    Jennifer
    Susan
    Hermann
    Shelley
      William
```
![](4.png)
## 实验总结和体会
在进行这次实验过程中，我学习了如何创建Oracle中的包、函数和过程，并且掌握了Oracle PL/SQL语言的基础知识。我掌握了以下内容：
1.包的概念及创建方法。在Oracle中，包是一种可以将存储过程、函数、变量、常量等封装在一起的结构。它可以提高代码的重用性和可维护性。在创建包时，需要使用CREATE PACKAGE和CREATE PACKAGE BODY两条SQL语句。
2.函数的概念及创建方法。函数是一种可以接收输入参数并返回一个值的程序单元。在Oracle中，函数是作为包的一部分被创建的。在创建函数时，需要在包体中使用CREATE FUNCTION语句，并在其中定义输入参数和返回值。
3.过程的概念及创建方法。过程是一种可以接收输入参数但不返回值的程序单元。在Oracle中，过程也是作为包的一部分被创建的。在创建过程时，需要在包体中使用CREATE PROCEDURE语句，并在其中定义输入参数和具体操作。<p>
在实验中，我通过创建包、函数和过程来实现了查询员工表中数据的功能。在创建函数时，我使用了SELECT语句来查询员工表中某个部门的工资总额。在创建过程时，我使用了游标来递归查询某个员工及其所有下属，子下属员工。<p>
通过本次实验，我不仅掌握了Oracle PL/SQL语言的基础知识，而且还深刻理解了封装性、重用性和可维护性等编程思想，这对我的编程能力和实际工作能力都有很大的提升。